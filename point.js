var Point= function(position, mass=1, velocity = new Vector(0,0)) {
	this.position = position;
	this.mass = mass;
	var radius = Math.round(Math.sqrt(mass))*1.5/* *10 */;
	if(radius<1){
		radius = 1;
	}
	this.radius = radius;
	this.velocity = velocity;
	this.acceleration = new Vector(0,0);
}

Point.prototype.updatePosition = function(delta){
	this.position.addTo( this.velocity.multiply(delta));
}

Point.prototype.distanceTo = function(point2){
	return this.position.subtract(point2.position).getLength() - (this.radius +point2.radius);
}