const delta = 1000/60;
const restart_time = 1000*10;

var canvas = document.getElementById("canvas");
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
var ctx = canvas.getContext("2d");

ctx.fillStyle="#000000";
ctx.beginPath();
ctx.fillRect(0, 0, canvas.width, canvas.height);

var mousePosition = new Vector(canvas.width/2, canvas.height/2);

function rndLog(size = 10) {
    //uniform random from 1 to 10^size
	var x = 1+Math.random()*(Math.exp(size)-1);
	return 1-(Math.log(x)/size);
}

function render(){
	//ctx.clearRect(0, 0, canvas.width, canvas.height);
	ctx.fillStyle="#000000";
	ctx.save();
	ctx.globalAlpha = 0.1;
	//ctx.globalCompositeOperation = "source-over";
	ctx.globalCompositeOperation = "darken";
	ctx.beginPath();
	ctx.fillRect(0, 0, canvas.width, canvas.height);
	ctx.restore();
	
	ctx.fillStyle="#FFFFFF";
	
	//update velocities
	points.map( 
		(point)=> {
			point.acceleration = points.reduce( 
				(acc, point2)=> {
					if(!Object.is(point, point2)){
						if(point.distanceTo(point2)<0){
							console.log("collision");
							return acc.add(point2.position.subtract(point.position).normalized().multiply(point.distanceTo(point2)/1000));
						}else{
							var point2ToPoint = point2.position.subtract(point.position);
							var r = point2ToPoint.getLength();
							var direction = point2ToPoint.normalized();
							return acc.add(direction.multiply(point2.mass/(10*r*r)));
						}
					}
					return acc
				},
				new Vector()
			)
			point.velocity.addTo(point.acceleration.multiply(delta))
		}
	)
	
	// update positions
	points.map( 
		(point)=> {
			point.updatePosition(delta);
		}
	)
	
	//draw points
	points.map( 
		(point)=> {
			ctx.beginPath();
			if(point.radius<1){
				ctx.fillRect(point.position.x, point.position.y, 1, 1);
			}else{
				ctx.arc(point.position.x, point.position.y, point.radius, 0, 2*Math.PI);
				ctx.fill();
			}
		}
	)
}

function restart(){
	const MAX_SPEED = 0.2;
	const MAX_MASS = 200;
	ctx.fillStyle="#000000";
	ctx.beginPath();
	ctx.fillRect(0, 0, canvas.width, canvas.height);
	points = [];
	for(i=0; i<2+10*Math.random(); i++){
		var position = new Vector(canvas.width*Math.random(), canvas.height*Math.random());
		var velocity = new Vector(MAX_SPEED*Math.random()-MAX_SPEED/2, MAX_SPEED*Math.random()-MAX_SPEED/2);
		var mass = MAX_MASS*rndLog();
		points.push(new Point(position, mass, velocity))
	}
}

function mouseUpdate(event){
	mousePosition = new Vector(event.clientX, event.clientY);
}

points = [
	// new Point(new Vector(600,300), 1, new Vector(0,0.43)),
	// new Point(new Vector(200,300), 20, new Vector(0,0.2)),
	// new Point(new Vector(900,300), 500, new Vector(0,-0.00886))
	new Point(new Vector(300,300), 50000, new Vector(0,-0.00886)),
	new Point(new Vector(900,300), 50000, new Vector(0,-0.00886))
]

restart();
//document.getElementById("canvas").addEventListener("mousemove", mouseUpdate);

window.setInterval(render, delta);
window.setInterval(restart, restart_time);